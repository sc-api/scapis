<?php

require('../classes/SCAPIS.class.php');

// Array of parameters for the query.
$params = array
(
	// Malformed params.
);

$result = \SCAPI\SCAPIS::SearchAccount($params);

if($result['success'])
{
	echo 'SUCCEEDED';
}
else
{
	echo 'FAILED';
}