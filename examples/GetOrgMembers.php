<?php

require('../classes/SCAPIS.class.php');

// Array of parameters for the query.
$params = array
(
    'page'			=>	1,
    'pagesize'		=>	25,
    'search'		=>	'', // Filter by handle.
    'symbol'		=>	'ARCSEC', // Filter by org SID.
);

var_dump(\SCAPI\SCAPIS::GetOrgMembers($params)['data']);