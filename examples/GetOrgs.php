<?php

require('../classes/SCAPIS.class.php');

// Array of parameters for the query.
$params = array
(
	'activity'		=>	'',
    'commitment'	=>	'',
    'language'		=>	'',
    'model'			=>	'',
    'page'			=>	1,
    'pagesize'		=>	25,
    'recruiting'	=>	'',
    'roleplay'		=>	'',
    'search'		=>	'',
    'size'			=>	'',
    'sort'			=>	'size_desc', // 'name_desc', 'name_asc', 'size_desc', 'size_asc', 'created_desc', 'created_asc', 'active_desc', 'active_asc'
);

var_dump(\SCAPI\SCAPIS::GetOrgs($params)['data']);