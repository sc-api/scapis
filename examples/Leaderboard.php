<?php

require('../classes/SCAPIS.class.php');

// Get org stats.

// Array of parameters for the query.
$params = array
(
	'map'           =>      'MAP-ANY',
	'mod'           =>      'BR',
	'pagesize'      =>      '25',
	'page'          =>      '1',
	'season'        =>      '1.1.5',
	'sort'			=>		'rank_score',
	'org'			=>		'ARCSEC',
	'type'			=>		'Org',
);

$result = \SCAPI\SCAPIS::GetLeaderboard($params)['data']['resultset'];
var_dump($result[array_search($params['org'], array_column($result, 'symbol'))]);



// Get account stats by org.

// Array of parameters for the query.
$params = array
(
	'map'           =>      'MAP-ANY',
	'mod'           =>      'BR',
	'pagesize'      =>      '25',
	'page'          =>      '1',
	'season'        =>      '1.1.5',
	'sort'			=>		'rank_score',
	'org'			=>		'ARCSEC',
	'type'			=>		'Account',
);

var_dump(\SCAPI\SCAPIS::GetLeaderboard($params)['data']['resultset']);



// Get account stats by handle.

// Array of parameters for the query.
$params = array
(
	'map'           =>      'MAP-ANY',
	'mod'           =>      'BR',
	'pagesize'      =>      '25',
	'page'          =>      '1',
	'season'        =>      '1.1.5',
	'sort'			=>		'rank_score',
	'handle'		=>		'Sir_Wesley',
	'type'			=>		'Account',
);

$result = \SCAPI\SCAPIS::GetLeaderboard($params)['data']['resultset'];
var_dump($result[array_search($params['handle'], array_column($result, 'nickname'))]);



// Get additional stats.

// Array of parameters for the query.
$params = array
(
	'map'           =>      'MAP-ANY',
	'mod'           =>      'BR',
	'season'        =>      '1.1.5',
	'handle'		=>		'Sir_Wesley',
	'type'			=>		'Account',
);

var_dump(\SCAPI\SCAPIS::GetAdditionalStats($params)['data']['resultset']);