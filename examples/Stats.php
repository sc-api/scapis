<?php

require('../classes/SCAPIS.class.php');

// Array of parameters for the query.
$params = array
(
	'alpha_slots'	=>	true,
    'chart'			=>	'week',
    'fans'			=>	true,
    'fleet'			=>	true,
    'funds'			=>	true,
);

var_dump(\SCAPI\SCAPIS::GetFundingStats($params)['data']);