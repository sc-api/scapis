<?php

require('../classes/SCAPIS.class.php');

// Array of parameters for the query.
$params = array
(
	//'handle'        =>      '',
	'org'        	=>      'ARCSEC',
	'map'           =>      'MAP-ANY',
	'mod'           =>      'BR',
	'pagesize'      =>      '50',
	'page'          =>      '1',
	'season'        =>      '1.1.6',
);

var_dump(\SCAPI\SCAPIS::GetPlayerStats($params));