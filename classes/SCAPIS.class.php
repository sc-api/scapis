<?php

namespace SCAPI;

include('CurlClient.class.php');

// Get the settings and assign them to our static class variable.
require_once(__DIR__.'/../settings.php');
SCAPIS::$SETTINGS = $_SETTINGS;

class SCAPIS
{
	public static $SETTINGS;
	
	public static $BASE_URL = 'https://robertsspaceindustries.com/api/';
	
	// Performs the network IO and returns the response text.
	public static function Query($url, $params, $urlParts = null)
	{
		// If we have parts to append...
		if(isset($urlParts))
		{
			// For each part given...
			foreach($urlParts as $part)
			{
				// Append it to the base url.
				$url .= '/' . $part;
			}
		}
		
		// Perform the query with the given parameters.
		$response = CurlClient::PostToPage
		(
			self::$BASE_URL . $url, 
			$params
		);
		
		// We should have received JSON text so 
		// decode it into an associative array.
		$array = json_decode
		(
			$response, 
			true
		);
		
		return $array;
	}
	
	
	
	
	/*
		LEADERBOARD
	*/
	
	public static function GetLeaderboard($params)
	{
		return self::Query('arena-commander/getLeaderboard', $params);
	}
	
	public static function GetPlayerStats($params)
	{
		return self::Query('arena-commander/getPlayerStats', $params);
	}
	
	public static function GetAdditionalStats($params)
	{
		return self::Query('arena-commander/getAdditionalStats', $params);
	}
	
	
	
	
	/*
		ACCOUNTS
	*/
	
	public static function SearchAccounts($params)
	{
		return self::Query('account/searchAccounts', $params);
	}
	
	
	
	
	/*
		FUNDING
	*/
	
	public static function GetFundingStats($params)
	{
		return self::Query('stats/getCrowdfundStats', $params);
	}
	
	
	
	
	/*
		HUB
	*/
	
	public static function GetSeries($params)
	{
		return self::Query('hub/getSeries', $params);
	}
	
	public static function GetCommlinkItems($params)
	{
		return self::Query('hub/getCommlinkItems', $params);
	}
	
	
	
	
	/*
		COMMUNITY
	*/
	
	public static function GetTrackedPosts($params)
	{
		return self::Query('community/getTrackedPosts', $params);
	}
	
	
	
	
	/*
		ORGANIZATIONS
	*/
	
	public static function GetOrgs($params)
	{
		return self::Query('orgs/getOrgs', $params);
	}
	
	public static function GetOrgMembers($params)
	{
		return self::Query('orgs/getOrgMembers', $params);
	}
	
	
	
	
	/*
		STARMAP
	*/
	
	public static function getStarSystems($params)
	{
		return self::Query('starmap/star-systems', $params);
	}
	
	public static function getStarSystem($params, $urlParts)
	{
		return self::Query('starmap/star-systems', $params, $urlParts);
	}
	
	public static function GetCelestialObject($params, $urlParts)
	{
		return self::Query('starmap/celestial-objects', $params, $urlParts);
	}
}