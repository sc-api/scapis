<?php

namespace SCAPI;

class CurlClient
{
	public static function PostToPage($url, $params, $custom_headers = array())
	{
        $cookie_file_path = __DIR__."/cookies.txt";
		
		$agent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0";
		
		$headers = $custom_headers;
		$headers[] = "Accept: */*";
		$headers[] = "Connection: Keep-Alive";
		
		// Begin curl.
		$ch = curl_init();
		
		// Basic curl options for all requests.
		curl_setopt($ch, CURLOPT_HTTPHEADER,  $headers);
		curl_setopt($ch, CURLOPT_HEADER,  0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		//curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
		//curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, null, '&'));
		
		curl_setopt($ch, CURLOPT_URL, $url);
		
		// Run request.
		$result = curl_exec($ch);
		
		// Close request.
		curl_close($ch);
		
		return $result;
	}
}